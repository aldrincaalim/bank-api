import { Client, Account } from "../src/entities";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDaoTextFile } from "../src/daos/client-dao-textfile-impl";
import { ClientDaoPostgres } from "../src/daos/client-dao-postgres";

const clientDAO:ClientDAO = new ClientDaoPostgres();

const testClient:Client = new Client(0, 'Jan', 'Ghoul', true);
const testAccount:Account = new Account(0, 4004004004, 1000, false, testClient.clientId, 1);

test("Create a new client", async () => {
    const result:Client = await clientDAO.createClient(testClient);
    expect(result.clientId).not.toBe(0);
});

test("Get client by id", async () => {
    let client:Client = new Client(0, "Jeff", "Mark", true);

    client = await clientDAO.createClient(client);

    let retrievedClient = await clientDAO.getClientById(client.clientId);
    expect(retrievedClient.clientId).toBe(client.clientId);
})

test("Get all clients", async() => {
    let client1 = new Client(0, 'Jenna', 'Fischer', true);
    let client2 = new Client(0, 'Angela', 'Kinsey', false);
    let client3 = new Client(0, 'Meredith', 'Stuart', true);
    let client4 = new Client(0, 'Erin', 'Hannon', false);

    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);
    await clientDAO.createClient(client3);
    await clientDAO.createClient(client4);

    const clients:Client[] = await clientDAO.getAllClients();

    expect(clients.length).toBeGreaterThanOrEqual(4);
});

test("Update client", async() => {
    let client = new Client(0, "Pam", "Beasely", false);
    client = await clientDAO.createClient(client);

    client.lastName = "Halpert";

    client = await clientDAO.updateClient(client);

    const updatedClient = await clientDAO.getClientById(client.clientId);

    expect(updatedClient.lastName).toBe("Halpert");
});

test("Delete client by ID", async() => {
    let client = new Client(0, 'Andy', 'Bernard', false);

    client = await clientDAO.createClient(client);

    const result:boolean = await clientDAO.deleteClientById(client.clientId);

    expect(result).toBeTruthy();
})