import {connection} from '../src/connection';

test("Should create a connection ", async() => {
    const result = await connection.query('select * from client'); // acts as the txt file that we used to grab info from but this time it is grabbing information from the cloud, specifically google cloud to our database in order to get the information
    // it's fetching the data stored on a database in the cloud
    console.log(result);
    // this connection will supply you the data object specifically the "rows" array that provides the array of objects, similar to that of the txt files we've worked with to grab data from locally
});

afterAll(() => {
    connection.end();
});