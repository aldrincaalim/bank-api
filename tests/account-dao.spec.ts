import { Account, Client } from "../src/entities";
import { AccountDAO } from "../src/daos/account-dao";
import { AccountDaoTextFile } from "../src/daos/account-dao-textfile-impl";
import { ClientDaoTextFile } from "../src/daos/client-dao-textfile-impl";
import { ClientDAO } from "../src/daos/client-dao";
/* 
PROBLEM WITH THIS FILE
    - the client's clientId does NOT match up with the account's clientId
*/

const clientDAO:ClientDAO = new ClientDaoTextFile();
const accountDAO:AccountDAO = new AccountDaoTextFile();

const testClient:Client = new Client(0, 'Billy', 'Bob', true);
const testAccount:Account = new Account(0, 4004004004, 1000, true, testClient.clientId, 0);

test("Create a new account", async() => {
    const result:Account = await accountDAO.createAccount(testAccount);
    expect(result.accountId).not.toBe(0);
});

test("Get account by ID", async() => {
    let client:Client = new Client(0, 'Michael', 'Scott', false);
    let account:Account = new Account(0, 1001001001, 62, false, client.clientId, 0);

    account = await accountDAO.createAccount(account);

    let retrievedAccount = await accountDAO.getAccountById(account.accountId);
    expect(retrievedAccount.accountId).toBe(account.accountId);
});

test("Get all accounts", async() => {
    let client1:Client = new Client(0, 'Oscar', 'Martinez', true);
    let account1:Account = new Account(0, 2002002002, 3000, true, client1.clientId, 0);
    let client2:Client = new Client(0, 'Kevin', 'Mallone', false);
    let account2:Account = new Account(0, 3003003003, 130, false, client2.clientId, 5);

    await accountDAO.createAccount(account1);
    await accountDAO.createAccount(account2);

    const accounts:Account[] = await accountDAO.getAllAccounts();

    expect(accounts.length).toBeGreaterThanOrEqual(2);
});

test("Update account", async() => {
    let client:Client = new Client(0, 'Dwight', 'Schrute', false);
    let account:Account = new Account(0, 9990009990, 10_000, true, client.clientId, 100);

    account = await accountDAO.createAccount(account);

    account.balance += 2_000;

    account = await accountDAO.updateAccount(account);

    const updatedAccount = await accountDAO.getAccountById(account.accountId);

    expect(updatedAccount.balance).toBe(12_000);
});

test("Delete account by ID", async() => {
    let client:Client = new Client(0, 'Kelly', 'Kapoor', false);
    let account:Account = new Account(0, 1110002220, 7_000, true, client.clientId, 10);

    account = await accountDAO.createAccount(account);

    const result:boolean = await accountDAO.deleteAccountById(account.accountId);
    console.log("Logging out result: " + result);
    expect(result).toBeTruthy();
});

