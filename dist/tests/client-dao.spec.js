"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var entities_1 = require("../src/entities");
var client_dao_postgres_1 = require("../src/daos/client-dao-postgres");
var clientDAO = new client_dao_postgres_1.ClientDaoPostgres();
var testClient = new entities_1.Client(0, 'Jan', 'Ghoul', true);
var testAccount = new entities_1.Account(0, 4004004004, 1000, false, testClient.clientId, 1);
test("Create a new client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, clientDAO.createClient(testClient)];
            case 1:
                result = _a.sent();
                expect(result.clientId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Get client by id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, retrievedClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Jeff", "Mark", true);
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientDAO.getClientById(client.clientId)];
            case 2:
                retrievedClient = _a.sent();
                expect(retrievedClient.clientId).toBe(client.clientId);
                return [2 /*return*/];
        }
    });
}); });
test("Get all clients", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client1, client2, client3, client4, clients;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client1 = new entities_1.Client(0, 'Jenna', 'Fischer', true);
                client2 = new entities_1.Client(0, 'Angela', 'Kinsey', false);
                client3 = new entities_1.Client(0, 'Meredith', 'Stuart', true);
                client4 = new entities_1.Client(0, 'Erin', 'Hannon', false);
                return [4 /*yield*/, clientDAO.createClient(client1)];
            case 1:
                _a.sent();
                return [4 /*yield*/, clientDAO.createClient(client2)];
            case 2:
                _a.sent();
                return [4 /*yield*/, clientDAO.createClient(client3)];
            case 3:
                _a.sent();
                return [4 /*yield*/, clientDAO.createClient(client4)];
            case 4:
                _a.sent();
                return [4 /*yield*/, clientDAO.getAllClients()];
            case 5:
                clients = _a.sent();
                expect(clients.length).toBeGreaterThanOrEqual(4);
                return [2 /*return*/];
        }
    });
}); });
test("Update client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, updatedClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Pam", "Beasely", false);
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                client.lastName = "Halpert";
                return [4 /*yield*/, clientDAO.updateClient(client)];
            case 2:
                client = _a.sent();
                return [4 /*yield*/, clientDAO.getClientById(client.clientId)];
            case 3:
                updatedClient = _a.sent();
                expect(updatedClient.lastName).toBe("Halpert");
                return [2 /*return*/];
        }
    });
}); });
test("Delete client by ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, 'Andy', 'Bernard', false);
                return [4 /*yield*/, clientDAO.createClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientDAO.deleteClientById(client.clientId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
