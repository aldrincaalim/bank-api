"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var entities_1 = require("../src/entities");
var client_service_impl_1 = require("../src/services/client-service-impl");
var clientService = new client_service_impl_1.ClientServiceImpl();
test("Register a new client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Ronald", "McDonald", true);
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                result = _a.sent();
                expect(result.clientId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Retrieve client by ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, retreivedClient;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Burger", "King", true);
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientService.retrieveClientById(client.clientId)];
            case 2:
                retreivedClient = _a.sent();
                expect(retreivedClient.clientId).toBe(client.clientId);
                return [2 /*return*/];
        }
    });
}); });
test("Retrieve list of all clients", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client1, client2, client3, clients;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client1 = new entities_1.Client(0, "Wendy", "Sassy", true);
                client2 = new entities_1.Client(0, "Chick", "Fil A", true);
                client3 = new entities_1.Client(0, "Panda", "Express", false);
                return [4 /*yield*/, clientService.registerClient(client1)];
            case 1:
                _a.sent();
                return [4 /*yield*/, clientService.registerClient(client2)];
            case 2:
                _a.sent();
                return [4 /*yield*/, clientService.registerClient(client3)];
            case 3:
                _a.sent();
                return [4 /*yield*/, clientService.retrieveAllClients()];
            case 4:
                clients = _a.sent();
                expect(clients.length).toBeGreaterThanOrEqual(3);
                return [2 /*return*/];
        }
    });
}); });
test("Updating client's investor status", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, updatedStatus;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Zaxby", "Chicken", false);
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                client = _a.sent();
                client.isInvestor = false;
                return [4 /*yield*/, clientService.updateClient(client)];
            case 2:
                client = _a.sent();
                return [4 /*yield*/, clientService.retrieveClientById(client.clientId)];
            case 3:
                updatedStatus = _a.sent();
                expect(updatedStatus.isInvestor).toBe(false);
                return [2 /*return*/];
        }
    });
}); });
test("Removing client", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, "Arby", "Meats", false);
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                client = _a.sent();
                return [4 /*yield*/, clientService.removeClientById(client.clientId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
