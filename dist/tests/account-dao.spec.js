"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var entities_1 = require("../src/entities");
var account_dao_textfile_impl_1 = require("../src/daos/account-dao-textfile-impl");
var client_dao_textfile_impl_1 = require("../src/daos/client-dao-textfile-impl");
/*
PROBLEM WITH THIS FILE
    - the client's clientId does NOT match up with the account's clientId
*/
var clientDAO = new client_dao_textfile_impl_1.ClientDaoTextFile();
var accountDAO = new account_dao_textfile_impl_1.AccountDaoTextFile();
var testClient = new entities_1.Client(0, 'Billy', 'Bob', true);
var testAccount = new entities_1.Account(0, 4004004004, 1000, true, testClient.clientId, 0);
test("Create a new account", function () { return __awaiter(void 0, void 0, void 0, function () {
    var result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, accountDAO.createAccount(testAccount)];
            case 1:
                result = _a.sent();
                expect(result.accountId).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Get account by ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, retrievedAccount;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, 'Michael', 'Scott', false);
                account = new entities_1.Account(0, 1001001001, 62, false, client.clientId, 0);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 1:
                account = _a.sent();
                return [4 /*yield*/, accountDAO.getAccountById(account.accountId)];
            case 2:
                retrievedAccount = _a.sent();
                expect(retrievedAccount.accountId).toBe(account.accountId);
                return [2 /*return*/];
        }
    });
}); });
test("Get all accounts", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client1, account1, client2, account2, accounts;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client1 = new entities_1.Client(0, 'Oscar', 'Martinez', true);
                account1 = new entities_1.Account(0, 2002002002, 3000, true, client1.clientId, 0);
                client2 = new entities_1.Client(0, 'Kevin', 'Mallone', false);
                account2 = new entities_1.Account(0, 3003003003, 130, false, client2.clientId, 5);
                return [4 /*yield*/, accountDAO.createAccount(account1)];
            case 1:
                _a.sent();
                return [4 /*yield*/, accountDAO.createAccount(account2)];
            case 2:
                _a.sent();
                return [4 /*yield*/, accountDAO.getAllAccounts()];
            case 3:
                accounts = _a.sent();
                expect(accounts.length).toBeGreaterThanOrEqual(2);
                return [2 /*return*/];
        }
    });
}); });
test("Update account", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, updatedAccount;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, 'Dwight', 'Schrute', false);
                account = new entities_1.Account(0, 9990009990, 10000, true, client.clientId, 100);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 1:
                account = _a.sent();
                account.balance += 2000;
                return [4 /*yield*/, accountDAO.updateAccount(account)];
            case 2:
                account = _a.sent();
                return [4 /*yield*/, accountDAO.getAccountById(account.accountId)];
            case 3:
                updatedAccount = _a.sent();
                expect(updatedAccount.balance).toBe(12000);
                return [2 /*return*/];
        }
    });
}); });
test("Delete account by ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var client, account, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = new entities_1.Client(0, 'Kelly', 'Kapoor', false);
                account = new entities_1.Account(0, 1110002220, 7000, true, client.clientId, 10);
                return [4 /*yield*/, accountDAO.createAccount(account)];
            case 1:
                account = _a.sent();
                return [4 /*yield*/, accountDAO.deleteAccountById(account.accountId)];
            case 2:
                result = _a.sent();
                console.log("Logging out result: " + result);
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
