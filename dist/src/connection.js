"use strict";
exports.__esModule = true;
exports.connection = void 0;
var pg_1 = require("pg");
require('dotenv').config({ path: 'C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\app.env' });
exports.connection = new pg_1.Client({
    user: 'postgres',
    password: process.env.DBPASSWORD,
    database: process.env.DATABASENAME,
    port: 5432,
    host: '34.86.19.9'
});
exports.connection.connect();
