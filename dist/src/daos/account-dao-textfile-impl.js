"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AccountDaoTextFile = void 0;
var promises_1 = require("fs/promises");
var AccountDaoTextFile = /** @class */ (function () {
    function AccountDaoTextFile() {
    }
    AccountDaoTextFile.prototype.createAccount = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        account.accountId = Math.round(Math.random() * 10000);
                        accounts.push(account);
                        return [4 /*yield*/, promises_1.writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts))];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoTextFile.prototype.getAllAccounts = function () {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        return [2 /*return*/, accounts];
                }
            });
        });
    };
    AccountDaoTextFile.prototype.getAccountById = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts, _i, accounts_1, account;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        for (_i = 0, accounts_1 = accounts; _i < accounts_1.length; _i++) {
                            account = accounts_1[_i];
                            if (account.accountId === accountId) {
                                return [2 /*return*/, account];
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // async getAccountByClientId(clientId: number): Promise<Account> {
    //     const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
    //     const textData:string = fileData.toString();
    //     const accounts:Account[] = JSON.parse(textData);
    //     for (const account of accounts) {
    //         if (account.clientId === clientId) {
    //             return account;
    //         }
    //     }
    // }
    AccountDaoTextFile.prototype.updateAccount = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        for (i = 0; i < accounts.length; i++) {
                            if (accounts[i].accountId === account.accountId) {
                                accounts[i] = account;
                            }
                        }
                        return [4 /*yield*/, promises_1.writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts))];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoTextFile.prototype.updateAccountBalance = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        for (i = 0; i < accounts.length; i++) {
                            if (accounts[i].accountId === account.accountId) {
                                accounts[i] = account;
                            }
                        }
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoTextFile.prototype.deleteAccountById = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var fileData, textData, accounts, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, promises_1.readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt')];
                    case 1:
                        fileData = _a.sent();
                        textData = fileData.toString();
                        accounts = JSON.parse(textData);
                        i = 0;
                        _a.label = 2;
                    case 2:
                        if (!(i < accounts.length)) return [3 /*break*/, 5];
                        if (!(accounts[i].accountId === accountId)) return [3 /*break*/, 4];
                        accounts.splice(i);
                        return [4 /*yield*/, promises_1.writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts))];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, true];
                    case 4:
                        i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, false];
                }
            });
        });
    };
    return AccountDaoTextFile;
}());
exports.AccountDaoTextFile = AccountDaoTextFile;
