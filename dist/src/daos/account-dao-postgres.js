"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.AccountDaoPostgres = void 0;
var entities_1 = require("../entities");
var connection_1 = require("../connection");
var errors_1 = require("../errors");
var AccountDaoPostgres = /** @class */ (function () {
    function AccountDaoPostgres() {
    }
    AccountDaoPostgres.prototype.createAccount = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "insert into account(account_number, balance, has_direct_deposit, amount) values ($1, $2, $3) returning account_id";
                        values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
                        return [4 /*yield*/, connection_1.connection.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        account.clientId = result.rows[0].clnt_id;
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoPostgres.prototype.getAllAccounts = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result, accounts, _i, _a, row, account;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sql = "select * from account";
                        return [4 /*yield*/, connection_1.connection.query(sql)];
                    case 1:
                        result = _b.sent();
                        accounts = [];
                        for (_i = 0, _a = result.rows; _i < _a.length; _i++) {
                            row = _a[_i];
                            account = new entities_1.Account(row.account_id, row.account_number, row.balance, row.has_direct_deposit, row.clnt_id, row.amount);
                            accounts.push(account);
                        }
                        return [2 /*return*/, accounts];
                }
            });
        });
    };
    // async getAccountByClientId(clientId: number): Promise<Account> {
    //     const sql:string = 'select * from account where clnt_id = $1';
    //     const values = [clientId];
    //     const result = await connection.query(sql, values);
    //     const accounts:Account[] = [];
    //     for (const row of result.rows) {
    //         const account:Account = new Account(row.account_id, row.account_number, row.balance, row.clnt_id);
    //         accounts.push(account);
    //     }
    //     return accounts;
    // }
    AccountDaoPostgres.prototype.getAccountById = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, row, account;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'select * from account where account_id = $1';
                        values = [accountId];
                        return [4 /*yield*/, connection_1.connection.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        row = result.rows[0];
                        account = new entities_1.Account(row.account_id, row.account_number, row.balance, row.has_direct_deposit, row.clnt_id, row.amount);
                        console.log(row.amount);
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoPostgres.prototype.updateAccount = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'update account set account_number=$1, balance=$2, has_direct_deposit=$3, amount=$4';
                        values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
                        return [4 /*yield*/, connection_1.connection.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The account with id " + account.accountId + " does not exist");
                        }
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoPostgres.prototype.updateAccountBalance = function (account) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'update account set account_number=$1, balance=$2, has_direct_deposit=$3, amount=$4';
                        values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
                        return [4 /*yield*/, connection_1.connection.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The account with id " + account.accountId + " does not exist");
                        }
                        return [2 /*return*/, account];
                }
            });
        });
    };
    AccountDaoPostgres.prototype.deleteAccountById = function (accountId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'delete from account where account_id = $1';
                        values = [accountId];
                        return [4 /*yield*/, connection_1.connection.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 1) {
                            throw new errors_1.MissingResourceError("The account with id " + accountId + " does not exist");
                        }
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return AccountDaoPostgres;
}());
exports.AccountDaoPostgres = AccountDaoPostgres;
