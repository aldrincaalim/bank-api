"use strict";
exports.__esModule = true;
exports.Account = exports.Client = void 0;
var Client = /** @class */ (function () {
    function Client(clientId, firstName, lastName, isInvestor) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isInvestor = isInvestor;
    }
    return Client;
}());
exports.Client = Client;
var Account = /** @class */ (function () {
    function Account(accountId, accountNumber, balance, hasDirectDeposit, clientId, amount) {
        this.accountId = accountId;
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.hasDirectDeposit = hasDirectDeposit;
        this.clientId = clientId;
        this.amount = amount;
    }
    return Account;
}());
exports.Account = Account;
