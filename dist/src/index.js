"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var account_service_impl_1 = require("./services/account-service-impl");
var client_service_impl_1 = require("./services/client-service-impl");
var errors_1 = require("./errors");
var app = express_1["default"]();
app.use(express_1["default"].json()); // Middleware
// Our application routes should use services we create to do the heavy lifting
// minimize the amount of logic in your routes routes that is NOT related directly to HTTP requests and responses
// Routes should only be ROUTES
var clientService = new client_service_impl_1.ClientServiceImpl();
var accountService = new account_service_impl_1.AccountServiceImpl();
app.post("/clients/:id/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var client;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = req.body;
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [2 /*return*/];
        }
    });
}); });
app.get("/clients", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clients;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, clientService.retrieveAllClients()];
            case 1:
                clients = _a.sent();
                res.send(clients);
                return [2 /*return*/];
        }
    });
}); });
app.get("/clients/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, clientService.retrieveClientById(clientId)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (error_1 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_1);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.post("/clients", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var client;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                client = req.body;
                return [4 /*yield*/, clientService.registerClient(client)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [2 /*return*/];
        }
    });
}); });
app.put("/clients/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, clientService.updateClientById(clientId)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                if (error_2 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_2);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/clients/:id/trueInvestor", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, clientService.updateClientInvestorStatusTrueById(clientId)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                if (error_3 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_3);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/clients/:id/falseInvestor", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, clientService.updateClientInvestorStatusFalseById(clientId)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_4);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// app.put("/clients/:id/changeName", async(req,res) => {
//     try {
//         const clientId = Number(req.params.id);
//         let client = await clientService.updateClientNameById(clientId);
//         res.send(client);
//     } catch(error) {
//         if (error instanceof MissingResourceError) {
//             res.status(404);
//             res.send(error);
//         }
//     }
// })
// app.put("/clients/:id/", async(req,res) => {
//     try{
//         const clientId = Number(req.params.id);
//         let client = await clientService.updateClientById(clientId);
//         res.send(client);
//     }catch(error) {
//         if (error instanceof MissingResourceError) {
//             res.status(404);
//             res.send(error);
//         }
//     }
// })
app["delete"]("/clients/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, client, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, clientService.removeClientById(clientId)];
            case 1:
                client = _a.sent();
                res.send(client);
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                if (error_5 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_5);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/clients/:id/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var clientId, account, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                clientId = Number(req.params.id);
                return [4 /*yield*/, accountService.retrieveAccountById(clientId)];
            case 1:
                account = _a.sent();
                console.log(account);
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_6 = _a.sent();
                if (error_6 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_6);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/accounts", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accounts, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, accountService.retrieveAllAccounts()];
            case 1:
                accounts = _a.sent();
                res.send(accounts);
                return [3 /*break*/, 3];
            case 2:
                error_7 = _a.sent();
                if (error_7 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_7);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/accounts/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.retrieveAccountById(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_8 = _a.sent();
                if (error_8 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_8);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/accounts/:id/directDepositTrue", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_9;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.updateAccountDirectDepositTrue(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_9 = _a.sent();
                if (error_9 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_9);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.patch("/accounts/:id/directDepositFalse", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.updateAccountDirectDepositFalse(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_10 = _a.sent();
                if (error_10 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_10);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.patch("/accounts/:id/deposit", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_11;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.depositAccountById(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_11 = _a.sent();
                if (error_11 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_11);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.patch("/accounts/:id/withdraw", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_12;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.withdrawAccountById(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_12 = _a.sent();
                if (error_12 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_12);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app["delete"]("/accounts/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var accountId, account, error_13;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                accountId = Number(req.params.id);
                return [4 /*yield*/, accountService.removeAccountById(accountId)];
            case 1:
                account = _a.sent();
                res.send(account);
                return [3 /*break*/, 3];
            case 2:
                error_13 = _a.sent();
                if (error_13 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_13);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(3000, function () {
    console.log("Application has started");
});
