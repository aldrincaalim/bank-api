import express from 'express';
import AccountService from './services/account-service';
import { AccountServiceImpl } from './services/account-service-impl';
import ClientService from './services/client-service';
import { ClientServiceImpl } from './services/client-service-impl';
import { Client, Account } from './entities';
import { MissingResourceError } from './errors';

const app = express();
app.use(express.json()); // Middleware

// Our application routes should use services we create to do the heavy lifting
// minimize the amount of logic in your routes routes that is NOT related directly to HTTP requests and responses
// Routes should only be ROUTES

const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

app.post("/clients/:id/accounts", async(req,res) => {
    try {
        let client:Client = req.body;
        client = await clientService.registerClient(client);
        res.send(client);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/clients", async(req, res) => {
    try {
        const clients:Client[] = await clientService.retrieveAllClients();
        res.send(clients);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
});

app.get("/clients/:id", async(req, res) => {    
    try {
        const clientId:number = Number(req.params.id);
        const client:Client = await clientService.retrieveClientById(clientId);
        res.send(client);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
});

app.post("/clients", async(req, res) => {
    try {
        let client:Client = req.body;
        client = await clientService.registerClient(client);
        res.send(client);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
});

app.put("/clients/:id", async(req,res) => {
    try {
        const clientId = Number(req.params.id);
        let client = await clientService.updateClientById(clientId);
        res.send(client);
    }catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.put("/clients/:id/trueInvestor", async(req,res) => {
    try {
        const clientId = Number(req.params.id);
        let client = await clientService.updateClientInvestorStatusTrueById(clientId);
        res.send(client);
    }catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.put("/clients/:id/falseInvestor", async(req,res) => {
    try {
        const clientId = Number(req.params.id);
        let client = await clientService.updateClientInvestorStatusFalseById(clientId);
        res.send(client);
    }catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

// app.put("/clients/:id/changeName", async(req,res) => {
//     try {
//         const clientId = Number(req.params.id);
//         let client = await clientService.updateClientNameById(clientId);
//         res.send(client);
//     } catch(error) {
//         if (error instanceof MissingResourceError) {
//             res.status(404);
//             res.send(error);
//         }
//     }
// })

// app.put("/clients/:id/", async(req,res) => {
//     try{
//         const clientId = Number(req.params.id);
//         let client = await clientService.updateClientById(clientId);
//         res.send(client);
//     }catch(error) {
//         if (error instanceof MissingResourceError) {
//             res.status(404);
//             res.send(error);
//         }
//     }
// })

app.delete("/clients/:id", async(req,res) => {
    try {
        const clientId = Number(req.params.id);
        let client = await clientService.removeClientById(clientId);
        res.send(client);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})



app.get("/clients/:id/accounts", async(req,res) => {
    try {
        const clientId = Number(req.params.id);
        const account:Account = await accountService.retrieveAccountById(clientId);
        console.log(account);
        res.send(account);
        // const client:Client = await clientService.retrieveClientById(clientId);
        // console.log(client);
        // res.send(client);
    }catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/accounts", async(req,res) => {
    try {
        const accounts:Account[] = await accountService.retrieveAllAccounts();
        res.send(accounts);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.get("/accounts/:id", async (req,res) => {
    try {
        const accountId: number = Number(req.params.id);
        const account:Account = await accountService.retrieveAccountById(accountId);
        res.send(account);
    } catch (error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.put("/accounts/:id/directDepositTrue", async(req,res) => {
    try {
        const accountId = Number(req.params.id);
        const account:Account = await accountService.updateAccountDirectDepositTrue(accountId);
        res.send(account);
    }catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
});

app.patch("/accounts/:id/directDepositFalse", async(req,res) => {
    try {
        const accountId = Number(req.params.id);
        const account:Account = await accountService.updateAccountDirectDepositFalse(accountId);
        res.send(account);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.patch("/accounts/:id/deposit", async(req,res) => {
    try {
        const accountId = Number(req.params.id);
        const account:Account = await accountService.depositAccountById(accountId);
        res.send(account);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.patch("/accounts/:id/withdraw", async(req,res) => {
    try {
        const accountId = Number(req.params.id);
        const account:Account = await accountService.withdrawAccountById(accountId);
        res.send(account);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})

app.delete("/accounts/:id", async(req,res) => {
    try {
        const accountId = Number(req.params.id);
        let account = await accountService.removeAccountById(accountId);
        res.send(account);
    } catch(error) {
        if (error instanceof MissingResourceError) {
            res.status(404);
            res.send(error);
        }
    }
})


app.listen(3000, () => {
    console.log(`Application has started`);
});