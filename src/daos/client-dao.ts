import {Client, Account} from "../entities";

export interface ClientDAO {
    // CREATE
    createClient(client:Client):Promise<Client>;

    // READ
    getAllClients():Promise<Client[]>;
    getClientById(clientId:number):Promise<Client>;

    // UPDATE
    updateClient(client: Client):Promise<Client>;
    // updateClient(client:Client):Promise<Client>;

    // DELETE
    deleteClientById(clientId:number):Promise<boolean>;

}