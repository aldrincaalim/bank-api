import { Account, Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { connection } from "../connection";
import { MissingResourceError } from "../errors";
import { AccountDAO } from "./account-dao";
import { ClientServiceImpl } from "../services/client-service-impl";

export class AccountDaoPostgres implements AccountDAO {
    async createAccount(account: Account): Promise<Account> {
        const sql: string = "insert into account(account_number, balance, has_direct_deposit, amount) values ($1, $2, $3) returning account_id";
        const values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
        const result = await connection.query(sql,values);
        account.clientId = result.rows[0].clnt_id;
        return account;
    }

    async getAllAccounts(): Promise<Account[]> {
        const sql:string = "select * from account";
        const result = await connection.query(sql);
        const accounts:Account[] = [];
        for (const row of result.rows) {
            const account:Account = new Account(row.account_id, row.account_number, row.balance, row.has_direct_deposit, row.clnt_id, row.amount);
            accounts.push(account);
        }
        return accounts;
    }
    
        // async getAccountByClientId(clientId: number): Promise<Account> {
            //     const sql:string = 'select * from account where clnt_id = $1';
        //     const values = [clientId];
        //     const result = await connection.query(sql, values);
        //     const accounts:Account[] = [];
        //     for (const row of result.rows) {
            //         const account:Account = new Account(row.account_id, row.account_number, row.balance, row.clnt_id);
            //         accounts.push(account);
            //     }
            //     return accounts;
            // }
            
    async getAccountById(accountId: number): Promise<Account> {
        const sql: string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await connection.query(sql, values);
        const row = result.rows[0];
        const account:Account = new Account(row.account_id, row.account_number, row.balance, row.has_direct_deposit, row.clnt_id, row.amount);
        console.log(row.amount);
        return account;
    }

    
    async updateAccount(account: Account): Promise<Account> {
        const sql:string = 'update account set account_number=$1, balance=$2, has_direct_deposit=$3, amount=$4';
        const values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
        const result = await connection.query(sql,values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The account with id ${account.accountId} does not exist`);
        }
        return account;
    }
    
    async updateAccountBalance(account:Account):Promise<Account> {
        const sql:string = 'update account set account_number=$1, balance=$2, has_direct_deposit=$3, amount=$4';
        const values = [account.accountNumber, account.balance, account.hasDirectDeposit, account.amount];
        const result = await connection.query(sql,values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The account with id ${account.accountId} does not exist`);
        }
        return account;
    }
            
    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1';
        const values = [accountId];
        const result = await connection.query(sql,values);
        if(result.rowCount === 1) {
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        return true;
    }
    
}