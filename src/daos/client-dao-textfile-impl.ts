import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { readFile,writeFile } from "fs/promises";
import { MissingResourceError } from "../errors";

export class ClientDaoTextFile implements ClientDAO {

    async createClient(client: Client): Promise<Client> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);
        client.clientId = Math.round(Math.random() * 10000);
        clients.push(client);
        await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt', JSON.stringify(clients));

        return client;
    }
    
    async getAllClients(): Promise<Client[]> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);

        return clients;
    }
    
    async getClientById(clientId: number): Promise<Client> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);
        
        for (const client of clients) {
            if (client.clientId === clientId) {
                return client;
            }
        }
        throw new MissingResourceError(`The client with id ${clientId} could not be located.`);
    }
    
    async updateClient(client: Client): Promise<Client> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);
        
        for (let i = 0; i < clients.length; i++) {
            if (clients[i].clientId === client.clientId) {
                clients[i] = client;
            }

        }
        
        await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt', JSON.stringify(clients));
        return client;
    }
    
    async deleteClientById(clientId: number): Promise<boolean> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].clientId === clientId) {
                clients.splice(i);
                await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\clients.txt', JSON.stringify(clients));
                return true;
            }
        }
        return false;
    }
    
}