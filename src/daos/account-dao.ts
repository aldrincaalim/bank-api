import {Account} from "../entities";

export interface AccountDAO {

    // CREATE
    createAccount(account:Account):Promise<Account>;

    // READ
    getAllAccounts():Promise<Account[]>;
    getAccountById(accountId:number):Promise<Account>;
    // getAccountByClientId(clientId:number):Promise<Account>;

    // UPDATE
    updateAccount(account:Account):Promise<Account>;
    updateAccountBalance(account:Account):Promise<Account>;
    
    // DELETE
    deleteAccountById(accountId:number):Promise<boolean>;
        
}