import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { readFile,writeFile } from "fs/promises";

export class AccountDaoTextFile implements AccountDAO {
    
    async createAccount(account: Account): Promise<Account> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        account.accountId = Math.round(Math.random() * 10000);
        accounts.push(account);
        await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts));
        
        return account;
    }
    
    async getAllAccounts(): Promise<Account[]> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        
        return accounts;
    }
    
    async getAccountById(accountId: number): Promise<Account> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        
        for (const account of accounts) {
            if (account.accountId === accountId) {
                return account;
            }
        }
    }
    
    // async getAccountByClientId(clientId: number): Promise<Account> {
    //     const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
    //     const textData:string = fileData.toString();
    //     const accounts:Account[] = JSON.parse(textData);

    //     for (const account of accounts) {
    //         if (account.clientId === clientId) {
    //             return account;
    //         }
    //     }
    // }

    async updateAccount(account: Account): Promise<Account> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId === account.accountId) {
                accounts[i] = account;
            }
        }
        await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts));
        
        return account;
        
    }

    async updateAccountBalance(account:Account):Promise<Account> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId === account.accountId) {
                accounts[i] = account;
            }
        }
        return account;
    }
    
    async deleteAccountById(accountId: number): Promise<boolean> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);
        
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId === accountId) {
                accounts.splice(i);
                await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt', JSON.stringify(accounts));
                return true;
            }
        }
        return false;
    }
    
}