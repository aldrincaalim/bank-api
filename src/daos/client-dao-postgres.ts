import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import {connection} from "../connection";
import { MissingResourceError } from "../errors";

export class ClientDaoPostgres implements ClientDAO {
    
    async createClient(client: Client): Promise<Client> {
        const sql: string = "insert into client(first_name, last_name, is_investor) values ($1, $2, $3) returning client_id";
        const values = [client.firstName, client.lastName, client.isInvestor];
        const result = await connection.query(sql, values);
        client.clientId = result.rows[0].client_id;
        return client;
    }

    async getAllClients(): Promise<Client[]> {
        const sql:string = "select * from client";
        const result = await connection.query(sql);
        const clients:Client[] = [];
        for (const row of result.rows) {
            const client:Client = new Client(row.client_id, row.first_name, row.last_name, row.is_investor);
            clients.push(client);        
        }
        return clients;

    }
    async getClientById(clientId: number): Promise<Client> {
        const sql:string = 'select * from client where client_id = $1';
        const values = [clientId];
        const result = await connection.query(sql, values);
        const row = result.rows[0];
        const client:Client = new Client(row.client_id, row.first_name, row.last_name, row.is_investor);
        return client;
        }

    async updateClient(client: Client): Promise<Client> {
        const sql:string = 'update client set first_name=$1, last_name=$2, is_investor=$3';
        const values = [client.firstName, client.lastName, client.isInvestor];
        const result = await connection.query(sql, values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The client with id ${client.clientId} does not exist`);
        }
        return client;

    }

    async deleteClientById(clientId: number): Promise<boolean> {
        const sql:string = 'delete from client where client_id = $1';
        const values = [clientId];
        const result = await connection.query(sql, values);
        if (result.rowCount === 0) {
            throw new MissingResourceError(`The client with id ${clientId} does not exist`);
        }
        return true;
    }
    
}