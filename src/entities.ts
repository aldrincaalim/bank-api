export class Client {
    constructor(
        public clientId:number,
        public firstName: string,
        public lastName: string,
        public isInvestor: boolean,
    ){}
}

export class Account {
    constructor(
        public accountId:number,
        public accountNumber:number,
        public balance:number,
        public hasDirectDeposit:boolean,
        public clientId:number,
        public amount: number
    ){}
}