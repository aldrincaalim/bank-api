import {Client} from "pg";
require('dotenv').config({path:'C:\\Users\\aldri\\Documents\\revature\\bank_api\\bank-api\\app.env'})

export const connection = new Client({
    user: 'postgres',
    password: process.env.DBPASSWORD, 
    database: process.env.DATABASENAME,
    port: 5432,
    host: '34.86.19.9'
});

connection.connect();