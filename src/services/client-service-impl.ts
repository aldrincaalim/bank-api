import { ClientDAO } from "../daos/client-dao";
import { ClientDaoTextFile } from "../daos/client-dao-textfile-impl";
import { Client } from "../entities";
import ClientService from "./client-service";
import { readFile,writeFile } from "fs/promises";
import { ClientDaoPostgres } from "../daos/client-dao-postgres";

export class ClientServiceImpl implements ClientService {
    
    clientDAO:ClientDAO = new ClientDaoPostgres();
    
    registerClient(client: Client): Promise<Client> {
        return this.clientDAO.createClient(client);
    }
    
    retrieveAllClients(): Promise<Client[]> {
        return this.clientDAO.getAllClients();        
    }
    
    retrieveClientById(clientId: number): Promise<Client> {
        return this.clientDAO.getClientById(clientId);
    }
    
    async updateClient(client: Client): Promise<Client> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\clients.txt');
        const textData:string = fileData.toString();
        const clients:Client[] = JSON.parse(textData);

        for (let i = 0; i < clients.length; i++) {
            if (clients[i].clientId === client.clientId) {
                clients[i] = client;
            }
        }
        
        await writeFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\clients.txt', JSON.stringify(clients));
        return client;
    }
    
    async updateClientInvestorStatusTrueById(clientId: number): Promise<Client> {
        let client:Client = await this.clientDAO.getClientById(clientId);
        client.isInvestor = true;
        client = await this.clientDAO.updateClient(client);
        return client;
    }
    
    async updateClientInvestorStatusFalseById(clientId: number): Promise<Client> {
        let client:Client = await this.clientDAO.getClientById(clientId);
        client.isInvestor = false;
        client = await this.clientDAO.updateClient(client);
        return client;
    }
    
    // async updateClientNameById(clientId: number): Promise<Client> {
    //     let client:Client = await this.clientDAO.getClientById(clientId);
    //     console.log(client);
    //     client.firstName = client.firstName;
    //     client.lastName = client.lastName;
    //     client = await this.clientDAO.updateClient(client);
    //     return client;
    // }
    
    async updateClientById(clientId:number):Promise<Client> {
        let client:Client = await this.clientDAO.getClientById(clientId);
        if (client.isInvestor === true) {
            client.isInvestor = false;
        } else {
            client.isInvestor = true;
        }
        
        client = await this.clientDAO.updateClient(client);
        return client;
    }

    async removeClientById(clientId: number): Promise<boolean> {
        let client:boolean = await this.clientDAO.deleteClientById(clientId);
        return client;
        }
    
}