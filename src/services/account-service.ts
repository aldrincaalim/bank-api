import { Account} from "../entities";

export default interface AccountService {
    registerAccount(account:Account):Promise<Account>;

    retrieveAllAccounts():Promise<Account[]>;

    retrieveAccountById(accountId:number):Promise<Account>;

    updateAccount(account:Account):Promise<Account>;

    updateAccountDirectDepositTrue(accountId:number):Promise<Account>;
    
    updateAccountDirectDepositFalse(accountId:number):Promise<Account>;
    
    depositAccountById(accountId:number):Promise<Account>;

    withdrawAccountById(accountId:number):Promise<Account>;
    
    updateAccountById(accountId:number):Promise<Account>;
    
    removeAccountById(accountId:number):Promise<boolean>;
}