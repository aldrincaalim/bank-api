import { Client } from "../entities";

export default interface ClientService {

    registerClient(client:Client):Promise<Client>;

    retrieveAllClients():Promise<Client[]>;

    retrieveClientById(clientId:number):Promise<Client>;

    updateClient(client: Client):Promise<Client>;

    updateClientById(clientId:number):Promise<Client>;

    updateClientInvestorStatusTrueById(clientId:number):Promise<Client>;

    updateClientInvestorStatusFalseById(clientId:number):Promise<Client>;
    
    // updateClientNameById(clientId:number):Promise<Client>;

    // updateClientInvestorStatusById(clientId:number):Promise<Client>;
    
    removeClientById(clientId:number):Promise<boolean>;

}