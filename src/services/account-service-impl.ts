import { AccountDAO } from "../daos/account-dao";
import { AccountDaoTextFile } from "../daos/account-dao-textfile-impl";
import { Account } from "../entities";
import AccountService from "./account-service";
import { readFile, writeFile } from "fs/promises";
import { write } from "fs";
import { AccountDaoPostgres } from "../daos/account-dao-postgres";

export class AccountServiceImpl implements AccountService {
    
    accountDAO:AccountDAO = new AccountDaoPostgres(); // change to AccountDaoPostgres();
    
    registerAccount(account: Account): Promise<Account> {
        return this.accountDAO.createAccount(account);
    }
    
    retrieveAllAccounts(): Promise<Account[]> {
        return this.accountDAO.getAllAccounts();
    }
    
    retrieveAccountById(accountId: number): Promise<Account> {
        return this.accountDAO.getAccountById(accountId);
    }
    
    updateAccountById(accountId: number): Promise<Account> {
        throw new Error("Method not implemented.");
    }

    async updateAccountDirectDepositTrue(accountId:number):Promise<Account> {
        let account:Account = await this.accountDAO.getAccountById(accountId);
        
            console.log(account.hasDirectDeposit);
            account.hasDirectDeposit = true;
            console.log(account.hasDirectDeposit);

            console.log(account);
            account = await this.accountDAO.updateAccount(account);
            console.log(account);
            return account;
        }

    async updateAccountDirectDepositFalse(accountId:number):Promise<Account> {
            let account:Account = await this.accountDAO.getAccountById(accountId);

            console.log(account);
            account.hasDirectDeposit = false; 
            console.log(account);

            account = await this.accountDAO.updateAccount(account);
            return account;
            }

    async depositAccountById(accountId:number):Promise<Account> {
        let account:Account = await this.accountDAO.getAccountById(accountId);

        console.log(account);

        account.balance += account.amount;

        console.log(account); 

        account = await this.accountDAO.updateAccount(account);
        return account;
    }
    async withdrawAccountById(accountId:number):Promise<Account> {
        let account:Account = await this.accountDAO.getAccountById(accountId);

        console.log(account);

        account.balance -= account.amount;

        console.log(account); 

        account = await this.accountDAO.updateAccount(account);
        return account;
    }
        
    async updateAccount(account: Account): Promise<Account> {
        const fileData:Buffer = await readFile('C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt');
        const textData:string = fileData.toString();
        const accounts:Account[] = JSON.parse(textData);

        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i].accountId === account.accountId) {
                accounts[i] = account;
            }
        }
        
        await writeFile("C:\\Users\\aldri\\Documents\\revature\\bank_api\\accounts.txt", JSON.stringify(accounts));
        return account;
        }
        
    async removeAccountById(accountId: number): Promise<boolean> {
        let account:boolean = await this.accountDAO.deleteAccountById(accountId);
        return account;
    }
    
}